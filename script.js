document.addEventListener('DOMContentLoaded', function() {
   const buttons = document.querySelectorAll('.btn'); 
   let activeButton = null; 

   function toggleButtonColor(button) {
     if (activeButton) {
       activeButton.style.backgroundColor = '#000000'; 
     }
     button.style.backgroundColor = '#007bff'; 
     activeButton = button; 
   }
 
   document.addEventListener('keydown', function(event) {
     const key = event.key.toUpperCase(); 
 
     const button = Array.from(buttons).find(btn => {
       const btnText = btn.textContent.trim().toUpperCase();
       if (btnText === 'ENTER' && key === 'ENTER') return true;
       if (btnText === 'TAB' && key === 'TAB') return true;
       return btnText === key;
     });
 
     if (button) {
       toggleButtonColor(button); 
     } else if (activeButton) {
       activeButton.style.backgroundColor = '#000000'; 
       activeButton.disabled = true; 
       activeButton = null;
     }
   });
 });
 
